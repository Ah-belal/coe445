function Calculate() {
    var c1 = parseInt(document.getElementById('cr1').value);
    var c2 = parseInt(document.getElementById('cr2').value);
    var g1 = document.getElementById('g1').value;
    var g2 = document.getElementById('g2').value;
    var w1 = convertLetterGrade(g1);
    var w2 = convertLetterGrade(g2);
    var eq1 = c1 * w1;
    var eq2 = c2 * w2;
    var totaleq = eq1 + eq2;
    var total = c1 + c2;
    document.getElementById("totalCredits").innerHTML = total;
    // Compute GPA here 
    gpa = totaleq / total;

    // print GPA
    document.getElementById("gpa").innerHTML = gpa; 
};
function convertLetterGrade(g) {
    if (g == "A+" || g == "a+"){
        return 4.00;
    }
    else if (g == "A" || g == "a"){
        return 3.75;
    }
    else if (g == "B+" || g == "b+"){
        return 3.50;
    }
    else if (g == "B" || g == "b"){
        return 3.00;
    }
    else if (g == "C+" || g == "c+"){
        return 2.50;
    }
    else if (g == "C" || g == "c"){
        return 2.00;
    }
    else if (g == "D+" || g == "d+"){
        return 1.50;
    }
    else if (g == "D" || g == "d"){
        return 1.00;
    }
    else {
        return 0.00;
    }
}